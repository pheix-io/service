#!/usr/bin/env bash

#Colors
RED='\e[0;31m'
GREEN='\e[0;32m'
YELLOW='\e[0;33m'
NC='\e[0m'
REDBOLD='\e[1;31m'

#Globals
ME=`basename "$0"`
AUTHADDR=$1
AUTHPASS=$2
HOST=pheix.webtech-msi
SESSTIME=45

AUTHRESPONSE=`curl --data "{\"credentials\":{\"login\":\"${AUTHADDR}\",\"password\":\"${AUTHPASS}\"},\"method\":\"GET\",\"httpstat\":\"200\",\"route\":\"/api/admin/auth\"}" -H "Content-Type: application/json" -X POST ${HOST}/api`

ACCESSTOKEN=`echo ${AUTHRESPONSE} | jq -r '.content.tparams.tx'`

if [ ! -z "${ACCESSTOKEN}" ] && [ "${ACCESSTOKEN}" != "null" ]; then
    while true; do
        echo -e "👌 ${GREEN}session is validated with ${ACCESSTOKEN} token for ${SESSTIME} seconds${NC}" && sleep ${SESSTIME}

        VELIDATERESPONSE=`curl --cookie "pheixauth=${ACCESSTOKEN}" --data '{"credentials":{"token":"0x0"},"method":"GET","httpstat":"200","route":"/api/admin/sess"}' -H "Content-Type: application/json" -X POST ${HOST}/api`

	    ACCESSTOKEN=`echo ${VELIDATERESPONSE} | jq -r '.content.tparams.tx'`
    done
else
    echo -e "⛔ ${REDBOLD}Authentication fails${NC}"
    exit 1;
fi

exit 0;
