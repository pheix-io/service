### Hi, Fran!

I have replied a few weeks ago, but had no answer. Then I noticed, that you gave me the wrong email via feedback form.

So, after a quick googling I have found the correct one.

Well, first I would like to thank you for contact, nice to know about guys with the same interests and ideas about future of web content and its management.

If you want to take part in Pheix project - u r welcome. If you can help this project and bring smth valuable to it - that will be great.

By the way, for me it looks like curious tech challenge, I want to keep it open and free.

But I also see a few much more interesting application areas for it, alongside or even instead of CMS. Actually CMS on blockchain (I mean Ethereum) is rather slow. Even if we use private networks with ultra fast servers, real-time for current implementation (Eth_v1) is unreachable, so this product has low performance and through-output from the birth.

Maybe Eth_v2 will be better, maybe not.

So, I think it's reasonable to focus on smth much more specific but relevant to requirements for CMS on blockchain.

Now I'm working on Ethelia.io project: this is REST API service for data storing on blockchain. The idea is quite simple (and genius): let's make a service that can store any content on private and/or public Ethereum networks. It's based on Pheix and provides API interface for end-users (so there could be plugins for Wordpress, Joomla, Dancer, Drupal, any CMS).

See my latest talk: https://narkhov.pro/multi-network-ethereum-dapp-in-raku.html

Service backend and frontend are free and open source, but initially service should has own hardware (or cloudware) infrastructure.

Service will be some kind of API gateway, but obviously can provide extended admin panel features, requests-per-month packages and similar options on non-free plans.

Also, if the end-user uses Ethereum Mainnet as the target database network, the service will take the operational fee per trx.

So, there are a lot of options to earn money and make a valuable project.

Well, if u want to join - welcome. Let me know how u see it, how u can improve it.

Also let me know a little bit more about u, what business did u sell, why and how u'r planning to start from the scratch.
