#!/bin/bash

SCHASH=0xcb4f4654006425fd0970a417534f8d3f311dfdc8
STXADDR=0xe54bacd9a9d408cefc5093f128a30eabeeaab45f91f4b1297e3498b87a10f19b
TOPICS=0x0d107ec98e10b891235d67d1a307feb1e54c15f8ac0a08839b06283aef9f1a8a

SUBSCIBEPORT=12345
PHEIX=/home/kostas/git/pheix-pool/core-perl6
DATASETS=t/data/datasets/set_09
SAVEPATH=${PHEIX}/$DATASETS
echo "Start listening on port ${SUBSCIBEPORT} ..."
while read line
do
    ACTIONCODE=`echo "${line}" | jq .params.result.topics[2]`
    if [ "${ACTIONCODE}" != 'null' ]; then
        CODE=`perl -le "print hex(${ACTIONCODE});"`

        # 5 is insert to blockchain code
        if [ "${CODE}" == '5' ]; then
            ROWID=`echo "${line}" | jq .params.result.topics[3]`
            ID=`perl -le "print hex(${ROWID});"`
            SAVEWD=`pwd`
            DATA=`raku unix-socket-adapter.raku node0 ${STXADDR} ${ID}`

            echo -n $DATA > ${SAVEPATH}/${ID}.txt && cd ${PHEIX}

            TTIME=`PHEIXTESTENGINE=1 PHEIXDEBUG=1 GOERLILOCALNODE=1 raku -Ilib ./t/06-blockchain-comp.t goerli_local deploy ./${DATASETS}/${ID}.txt trc2021/public/ skip | perl -lne 'print $1 if /Total time: \| ([0-9\.]+)/i'`

            cd ${SAVEWD} && echo data for ${ROWID} record is saved to Goerli testnet in ${TTIME} seconds
        fi
    fi
done < <((echo "{\"id\": 1, \"method\": \"eth_subscribe\", \"params\": [\"logs\", {\"address\": \"${SCHASH}\", \"topics\": [\"${TOPICS}\"]}]}") | nc --no-shutdown 127.0.0.1 ${SUBSCIBEPORT})
echo "Good bye"
