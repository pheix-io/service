use v6.d;
use Net::Ethereum;

my Str  $unlockpassw = @*ARGS[0] // 'node0';
my Str  $transact    = @*ARGS[1] // ( '0x' ~ '0' x 64 );
my UInt $rowid       = +(@*ARGS[2]) // 0;

my $obj = Net::Ethereum.new(
    api_url => 'http://127.0.0.1:8540',
    show_progress => False,
    unlockpwd     => $unlockpassw,
);

$obj.abi = '/home/kostas/git/pheix-pool/core-perl6/www/conf/system/abi/PheixDatabase.abi'.IO.slurp.Str;

$obj.contract_id = $obj.retrieve_contract($transact);

$obj.personal_unlockAccount;

my $data = $obj.contract_method_call(
    'select',
    %(tabname => "trc2021/1628023227", rowid => $rowid)
)<data>;

say $data if $data and $data ne q{};
