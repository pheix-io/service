# Subscription service

Geth subscription details: [https://geth.ethereum.org/docs/rpc/pubsub](https://geth.ethereum.org/docs/rpc/pubsub).

## IPC sockets in Raku

1. [Use](https://stackoverflow.com/questions/46221935/is-there-a-way-to-deal-with-unix-sockets-in-perl-6) perl5 module `IO::Socket::UNIX` via `Inline::Perl5` (see details at [github:niner](https://github.com/niner/Inline-Perl5));
2. Pure Raku implementation: already merged and integrated: [PR](https://github.com/rakudo/rakudo/pull/3357), [reddit discussion](https://www.reddit.com/r/rakulang/comments/eou2zy/how_do_you_use_file_system_sockets/fei1azm?utm_source=share&utm_medium=web2x&context=3);
3. How pure Raku implementation works at `CheckSocket` module: [source code](https://github.com/jonathanstowe/CheckSocket/blob/13f88d97486475d7dc18ef5e4b334e6707d2a48e/lib/CheckSocket.pm#L74).

## IPC sockets in Docker

1. [IPC communication between Docker containers](https://stackoverflow.com/a/44291769);
2. [Docker IPC settings](https://docs.docker.com/engine/reference/run/#ipc-settings---ipc).

### R&D

Some implementation details: [https://gitlab.com/pheix-io/service/-/issues/2](https://gitlab.com/pheix-io/service/-/issues/2).

#### Prototype

Subscription service is based on:

The prototype consists of [starter bash script](/files/subscription/unix-socket.bash), simple [Raku helper script](/files/subscription/unix-socket-adapter.raku) for private PoA node requesting and [public node communication Raku script](https://gitlab.com/pheix-pool/core-perl6/-/blob/develop/t/06-blockchain-comp.t) that uses a few Pheix submodules and methods.
Starter bash script has extremely simple logic, it works in infinitive duty loop with a few stages:

* subscribe to Geth client events like storing on blockchain with specific topics;
* filter events by metadata: table name or operation code;
* execute the helper scripts on event metadata matching: fetch data from private blockchain and transmit it to public one transparently;

Prototype could be run in demonized mode could be done with `nohup` command: `$ nohup unix-socket.bash &`.

**Starter bash script**

```bash
#!/bin/bash

SCHASH=0xcb4f4654006425fd0970a417534f8d3f311dfdc8
STXADDR=0xe54bacd9a9d408cefc5093f128a30eabeeaab45f91f4b1297e3498b87a10f19b
TOPICS=0x0d107ec98e10b891235d67d1a307feb1e54c15f8ac0a08839b06283aef9f1a8a

SUBSCIBEPORT=12345
PHEIX=/home/kostas/git/pheix-pool/core-perl6
DATASETS=t/data/datasets/set_09
SAVEPATH=${PHEIX}/$DATASETS

echo "Start listening on port ${SUBSCIBEPORT} ..."

while read line
do
    ACTIONCODE=`echo "${line}" | jq .params.result.topics[2]`

    if [ "${ACTIONCODE}" != 'null' ]; then
        CODE=`perl -le "print hex(${ACTIONCODE});"`

        # 5 is insert to blockchain code
        if [ "${CODE}" == '5' ]; then
            ROWID=`echo "${line}" | jq .params.result.topics[3]`
            ID=`perl -le "print hex(${ROWID});"`
            SAVEWD=`pwd`
            DATA=`raku unix-socket-adapter.raku node0 ${STXADDR} ${ID}`

            echo -n $DATA > ${SAVEPATH}/${ID}.txt && cd ${PHEIX}

            TTIME=`PHEIXTESTENGINE=1 PHEIXDEBUG=1 GOERLILOCALNODE=1 raku -Ilib ./t/06-blockchain-comp.t goerli_local deploy ./${DATASETS}/${ID}.txt trc2021/public/ skip | perl -lne 'print $1 if /Total time: \| ([0-9\.]+)/i'`

            cd ${SAVEWD} && echo data for ${ROWID} record is saved to Goerli testnet in ${TTIME} seconds
        fi
    fi
done < <((echo "{\"id\": 1, \"method\": \"eth_subscribe\", \"params\": [\"logs\", {\"address\": \"${SCHASH}\", \"topics\": [\"${TOPICS}\"]}]}") | nc --no-shutdown 127.0.0.1 ${SUBSCIBEPORT})
echo "Good bye"
```

**Raku helper script for private PoA node requests**

```perl
use v6.d;
use Net::Ethereum;

my Str  $unlockpassw = @*ARGS[0] // 'node0';
my Str  $transact    = @*ARGS[1] // ( '0x' ~ '0' x 64 );
my UInt $rowid       = +(@*ARGS[2]) // 0;

my $obj = Net::Ethereum.new(
    api_url => 'http://127.0.0.1:8540',
    show_progress => False,
    unlockpwd     => $unlockpassw,
);

$obj.abi = '/home/kostas/git/pheix-pool/core-perl6/www/conf/system/abi/PheixDatabase.abi'.IO.slurp.Str;

$obj.contract_id = $obj.retrieve_contract($transact);

$obj.personal_unlockAccount;

my $data = $obj.contract_method_call(
    'select',
    %(tabname => "trc2021/1628023227", rowid => $rowid)
)<data>;

say $data if $data and $data ne q{};
```

**Public node communication Raku script**

Public node communication Raku script is **Pheix** `./t/06-blockchain-comp.t` test, run with the next command line arguments:

```bash
PHEIXTESTENGINE=1 PHEIXDEBUG=1 GOERLILOCALNODE=1 raku -Ilib ./t/06-blockchain-comp.t goerli_local deploy ./${DATASETS}/${ID}.txt trc2021/public/ skip | perl -lne 'print $1 if /Total time: \| ([0-9\.]+)/i'
```

<span style="color:#fff; background-color:#ff9d00; padding:5px;">**WIP**</span>: add repo to [pheix-research](https://gitlab.com/pheix-research) group and add a test sample.
