# Service modules: Pheix::Addons::IO::API

Specification of `Pheix::Addons::IO::API` service module.

## Routes

### Generic route

`Pheix::Addons::IO::API` should extend Pheix routes with generic `/io` route, which supports multiple request methods:

* `GET` — fetch data from blockchain;
* `POST` — store new data on blockchain;
* `PATCH` — update existing data on blockchain;
* `DELETE` — delete existing data from blockchain.

#### Methods

* `GET` should provide `table_name` and `row_id`;
* `DELETE` should provide `table_name` and `row_id` (optional, if not given all records in `table_name` will be deleted);
* `PATCH` should provide `table_name`, `compression`, `row_id` and `data`;
* `POST` should provide `table_name`, `compression` and `data`.

### Service route

`Pheix::Addons::IO::API` should extend Pheix routes with service `/io/service` route, which supports single request method:

* `GET` — fetch service data from blockchain.

#### Payload

* `GET` should provide `table_name`, `row_id` (optional, required for some requests) and one of service commands:
    - `countRows` — get number of rows for given table;
    - `getFields` — get table legend;
    - `getTableModTime` — get table latest modification time;
    - `getMaxId` — get max row id for given table;
    - `getTableIndex`— get table in database vector;
    - `isTabCompressed` — get comp info for given table;
    - `idExists` — check if row with given id is existed in given table;
    - `isRecCompressed` — get comp info for row with given id is existed in given table;
    - `tableExists` — check if given table exists;
    - `getIdByIndex` — get row id by index in record vector for given table.

#### Example requests

##### Generic

*NotaBene*: `"httpstat"` and `"message"` are used for passing error status and message to content API: if controller catch `400` error, we retrieve error content and pass error message to content block. Well, at Ethelia.io service we have no explicit controller layer and work with API, so now it looks line we can skip `"httpstat"` and `"message"` usage.  

```javascript
{
  "token": "94a08da1fecbb6e8b46990538c7b50b2",
  "method": "GET",
  "route": "/io",
  "payload": {
    "table_name": "device-1-logs",
    "row_id": "1"
  }
}
```

```javascript
{
  "token": "94a08da1fecbb6e8b46990538c7b50b2",
  "method": "PATCH",
  "route": "/io",
  "payload": {
    "table_name": "device-1-logs",
    "compression": "1"
    "row_id": "2",
    "data": "[info]: device is online"
  }
}
```

##### Service

```javascript
{
  "token": "94a08da1fecbb6e8b46990538c7b50b2",
  "method": "GET",
  "route": "/io/service",
  "payload": {
    "table_name": "device-1-logs",
    "query": "countRows"
  }
}
```

```javascript
{
  "token": "94a08da1fecbb6e8b46990538c7b50b2",
  "method": "GET",
  "route": "/io/service",
  "payload": {
    "table_name": "device-1-logs",
    "query": "idExists",
    "row_id": "1"
  }
}
```
## APIs

Module `Pheix::Addons::IO::API` should has typical API structure. All module API calls could be classified to:

* required by Pheix CMS;
* required by module logic:
    - route handlers;
    - public helpers;
    - private helpers.

### Pheix CMS API calls

* `init_json_config()` — initializes addon config and stores it to private attribute `$!jsonobj`;
* `get()` — stores basic controller object to public attribute `$.ctrl` and returns `self`;
* `get_class()` — returns `self.^name`;
* `get_name()` — returns addon name: `IO`;
* `get_sm()` — returns empty List `List.new`, cause there's no content pages provided by this module;
* `fill_seodata()` — returns empty Hash `Hash.new`, cause there's no content pages provided by this module.

### Module logic API calls

#### Route handlers

* `browse_api_get_io()` — returns log record by `row_id`, if `row_id == 0` returns last 50 records from database via helper method `fast_select_all()`;  
* `browse_api_patch_io()` — updates data record with given `row_id`, no helpers used;
* `browse_api_delete_io()` — deleted data record with given `row_id`, if `row_id == 0` drops entire table via helper method `drop_table()`;  
* `browse_api_post_io()` — add data record to table, if table is not existed it should be created;
* `browse_api_get_service()` — returns service data by service command via helper method `service_request()`;  

#### Public helpers

* `error()` — outputs error response.

#### Private helpers

* `drop_table()` — drops entire table (implementations: [JavaScript](https://gitlab.com/pheix-research/smart-contracts/-/blob/master/t/PheixDatabase/pheix_database_init_drop.js#L60), [Raku](https://gitlab.com/pheix-pool/core-perl6/-/blob/develop/lib/Pheix/Test/Blockchain.rakumod#L167))
* `fast_select_all()` — returns last 50 records from database (implementations: [Raku](https://gitlab.com/pheix-pool/core-perl6/-/blob/develop/lib/Pheix/Test/Blockchain.rakumod#L258));
* `service_request()`  — returns service data by service command (see [Payload](#payload) section).

## Generic responses

There two generic responses:

* status response;
* data response.

### Status response

Status response is sent for `DELETE`, `PATCH` and `POST` requests. Common status response looks like:

```javascript
{
  "session": "9eb9e2fb-56ea-4e23-9da3-3520dc9a53cb",
  "status": "1",
  "details": {
    "rendertime": "0.00129",
    "transaction": "0xd325a638c34e8e232143113971b1925091105915d420a20abfc62ef7feb69eee",
    "error": ""
  }
}
```

```javascript
{
  "session": "f2370eac-9c3a-47cd-959f-8e4b42bac850",
  "status": "0",
  "details": {
    "rendertime": "0.0499",
    "transaction": "",
    "error": "invalid tx hash after sendTransaction <0x0>"
  }
}
```

### Data response

Data response is sent for `GET`. Common data response looks like:

```javascript
{
  "session": "36f071bd-0d05-4c17-9083-446bf4a76984",
  "status": "1",
  "details": {
    "rendertime": "0.00187",
    "data": "M2UxZTZhMTMtNGRmMy00ZDlmLWEwMDAtYjhiYmQ1NGE2YjUy",
    "error": ""
  }
}
```

```javascript
{
  "session": "8985eca3-5225-4def-aa7a-03917f36eed9",
  "status": "0",
  "details": {
    "rendertime": "0.032",
    "data": "",
    "error": "Not enough positional arguments; needed at least 4 in block at ./t/03-blockchain-common.t line 54"
  }
}
```

## License information

Pheix and its modules are free and open source software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Contributor

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
