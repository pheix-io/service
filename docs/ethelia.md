# Infrastructure

I suppose **ethelia.io** project as alias for **PHEIX.io**: the idea is to use different names for CMS and its use cases. Also possible donations and grants should be given to **service**. CMS should be independent, free and open source.

## Service grant details

We need road map for service life cycle. I suppose 1 year for start period and 1/2 year for setup, debug, tune, etc...

So for the first 2 years we need to be sponsored:

* server collocation: €50 per month, total €1200 for 2 years;
* server upgrade: 4 enterprise SSD hard disks, €150 - total €600;
* ethelia.io domain: €120 for 2 years;
* ethelia.io site render/layout (based on @KKonopleva brand book): €1000;
* ethelia.io documentation issue: €500;
* ethelia.io smart contract and source code audit: €1500;

Total: €4920

### What is the pre-start (current) state

We have the next initial things:

* start level server, based on [Tyan S7010](https://www.tyan.com/Motherboards_S7010_S7010AGM2NRF) with double 6 core (12 threads) Intel Xeon CPUs and 96Gb RAM;
* server is setup: Ubuntu 20.04 server with standard set of services we needed, so it could be used right now with performance risks (non enterprise HDDs, no RAID);
* service [brand book](https://gitlab.com/pheix-pool/core-perl6/-/issues/68);
* Pheix at public beta version, it could be used as a prototype and should works as demo for potential investors;

## Start period road map

```mermaid
graph TD;

A[Run pre-start/current state service]-->B[Start funding]
B-->SubGraph2Flow;
SubGraph2D-->C[Post period: +1/2 year after we got investments];

  subgraph "Got investments"
      SubGraph2Flow(Upgrade hardware collocate it and suppose 99.9% up-time)
      SubGraph2Flow --> SubGraph2A[Issue the documentation]
      SubGraph2A --> SubGraph2B[Perform audit round for smart contracts and Raku sources]
      SubGraph2B --> SubGraph2C[Create web site and open it for private beta-testing]
      SubGraph2C --> SubGraph2D[Start the service - make it available via web]
  end
```

## Grant issuer

https://www.oscollective.org/
