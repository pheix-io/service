# Sessions

## How it works at Perl5 Pheix

1. [Auth](https://gitlab.com/pheix-pool/core/-/blob/master/www/admin.pl#L72) by login and password, `getDecryptUid` sub [does](https://gitlab.com/pheix-pool/core/-/blob/master/www/admin/libs/modules/Pheix/Session.pm#L166):
    - split `$data_from_file` to elements, input is `userid|date|<crypted-fio>|<crypted-login>|md5_hex( <keytag> . <passw> )|<keytag>`;
    - decrypt `crypted-login` with `keytag`;
    - join password hash with `md5_hex($keytag . $passw)`;
    - check if decrypted login and password hash are equal to input ones;
    - check permissions;
    - returns `userid` on success.
2. [Clean](https://gitlab.com/pheix-pool/core/-/blob/master/www/admin/libs/modules/Pheix/Session.pm#L255) existed sessions with `delSessionByTimeOut`, records in session storage are: `id|ip_addr|session_id|mod_time`, we are deleting all records with `mod_time` older than 15 minutes;
3. [Create](https://gitlab.com/pheix-pool/core/-/blob/master/www/admin/libs/modules/Pheix/Session.pm#L200) new session with `createUserSession` for given `userid`;
4. On every next run we [call](https://gitlab.com/pheix-pool/core/-/blob/master/www/admin.pl#L106) `isValidUser` for session [check](https://gitlab.com/pheix-pool/core/-/blob/master/www/admin/libs/modules/Pheix/Session.pm#L340), if check is successful we [store current time](https://gitlab.com/pheix-pool/core/-/blob/master/www/admin/libs/modules/Pheix/Session.pm#L343) as session `mod_time`;
5. On exit we ask for a current password and if it's given we [update](https://gitlab.com/pheix-pool/core/-/blob/master/www/admin/libs/modules/Secure/UserAccess.pm#L1155) the user keytag/hashes:
    - generate new keytag: `md5_hex( rand() )`;
    - crypt login and firstname/lastname;
    - generate new password hash with `md5_hex( $keytagnew . $passwd )`.

### Critics

1. `keytag` is stored in plain text, so if we retrieve it from server we can decrypt everything we want and try to get password with brute force on `md5_hex($keytag . $passw)`;
2. It's reasonable to update hashes also on log in (not only on exit).

### Attacks

1. Steal the `session` and mock IP address;
2. [CSRF](https://en.wikipedia.org/wiki/Cross-site_request_forgery).

## How it should work at Pheix/Ethelia (raku)

Notabene: [https://helda.helsinki.fi/bitstream/handle/10138/228842/aaa-ethereum-blockchain.pdf](https://helda.helsinki.fi/bitstream/handle/10138/228842/aaa-ethereum-blockchain.pdf).

1. Auth by address (login) and password on Ethereum node with `personal_unlockAccount`;
2. <span style="color:#fff; background-color:#ff0018; padding:5px;">**TBD**</span>
