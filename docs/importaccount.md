# Import account to local PoA network node

## Add account from existed private key

Details about how to add account from existed private key: [auth.md](auth.md). The fastest way to import account, created with MetaMask or other wallets that are providing private key export.

## Import from keystore file

* save account keystore file locally to, for example, `9dc4c65c12d81f741ae57ac7abc3a02a342625a4.json`;
* run parity locally;
* run import bash script:

```bash
#!/bin/bash

FNAME=/home/kostas/Documents/auth/rinkeby/9dc4c65c12d81f741ae57ac7abc3a02a342625a4.json
KEYSTORE=`cat ${FNAME} | sed 's/"/\\\\"/g'`
PWD='account-password'

DATA=('{"method":"parity_newAccountFromWallet","params":["'${KEYSTORE}'","'${PWD}'"],"id":1,"jsonrpc":"2.0"}');

curl -s --data ${DATA[0]} -H "Content-Type: application/json" -X POST localhost:8541 | jq
```

* get successful response:

```javascript
{
  "jsonrpc": "2.0",
  "result": "0x9dc4c65c12d81f741ae57ac7abc3a02a342625a4",
  "id": 1
}
```

## Create keystore file from account private key

* get account private key, for example, `4895d93fc350d40b2d7c181b4e3b2a01fa7f2dacf1bc4fa7a8a68782fd3af6e5`;
* run `nodejs`:

```javascript
// https://ethereum.stackexchange.com/questions/16797/export-metamask-account-to-json-file/92187

const keystring = '4895d93fc350d40b2d7c181b4e3b2a01fa7f2dacf1bc4fa7a8a68782fd3af6e5'
const fs = require('fs');
const wallet = require('ethereumjs-wallet').default;

const pk = new Buffer.from(keystring, 'hex');
const account = wallet.fromPrivateKey(pk);
const password = 'account-password';
account.toV3(password)
    .then(value => {
        const address = account.getAddress().toString('hex')
        const file = `UTC--${new Date().toISOString().replace(/[:]/g, '-')}--${address}.json`
        fs.writeFileSync(file, JSON.stringify(value))
    });
```

* check the saved file `UTC--xxxx-9dc4c65c12d81f741ae57ac7abc3a02a342625a4.json`:

```javascript
{
  "version": 3,
  "id": "6560dea6-e599-43db-bd09-92eb7bdc73a0",
  "address": "9dc4c65c12d81f741ae57ac7abc3a02a342625a4",
  "crypto": {
    "ciphertext": "158ce2247b3597fa88179b597465b6c9924d02ab597ca6a5b66a17d683d61c67",
    "cipherparams": {
      "iv": "96b5aa9928be9e38b21c94ad42ed0154"
    },
    "cipher": "aes-128-ctr",
    "kdf": "scrypt",
    "kdfparams": {
      "dklen": 32,
      "salt": "ac88196315458f498ab9d7c9841e72fda3f31b08ad22aa4369a9303e594d5cdb",
      "n": 262144,
      "r": 8,
      "p": 1
    },
    "mac": "4113c77a353cc9dd640da3de81b5476bf84e11f149d9d592b24fd42ef21138b0"
  }
}
```

* import this keystore file to local PoA network node (see above).
