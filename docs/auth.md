# Authentication problems

It's ok if we work with local/private PoA networks: we use auth node (one of the network nodes and perform authentication there with `eth.personal.unlockAccount`).

## infura.io

[infura.io](https://infura.io) is used for relaying the transactions to test networks. Unfortunately it does not support any accounts on its nodes.

If we have no account management on the node, we should:

* manage accounts/users/roles on service side;
* sign transactions locally.

Account management could be performed with Pheix CMS (as it's done in perl5 Pheix), but signing transactions is much more tricky. Here's the problem description: [https://gitlab.com/pheix/net-ethereum-perl6/-/issues/24](https://gitlab.com/pheix/net-ethereum-perl6/-/issues/24).

## Actual state

Implementation of signed transactions at `Net::Ethereum` has high costs. So we need to manage this other way. For example: we should try to sign transactions locally with local node (run under Docker).

The path is:

```mermaid
graph TD;

subgraph "Auth with local PoA node"
    SubGraph2Flow(Send query for TX signing to locally running at docker PoA network)
    SubGraph2Flow --> SubGraph2A[Got the signed data]
    SubGraph2A --> SubGraph2B[Send signed data to infura.io]
end
```

##### Get private key details `$ sudo cat <keystore file> | jq`

```javascript
{
  "address": "cdf4e0481e796afae76a9e4c537d4b895925b0cc",
  "crypto": {
    "cipher": "aes-128-ctr",
    "ciphertext": "20eefb63e134b84067070485c30a49bbbf7add372974ade055c9ff7385c8e678",
    "cipherparams": {
      "iv": "a3de849989e8ab0f0e204c2af6ebe890"
    },
    "kdf": "scrypt",
    "kdfparams": {
      "dklen": 32,
      "n": 262144,
      "p": 1,
      "r": 8,
      "salt": "2d06aa483f1c7dc36bddb188b449b87868798a03679227254453e92e7385ab18"
    },
    "mac": "3d8126c1a1602e5b6dbc4548dbebe2bdd5e028009b4d5b59925127b2422b434a"
  },
  "id": "23d172db-7a72-464f-b5f0-9d57c769b448",
  "version": 3
}
```

##### Decrypt private key with `python3`

Source: [https://ethereum.stackexchange.com/questions/3720/how-do-i-get-the-raw-private-key-from-my-mist-keystore-file](https://ethereum.stackexchange.com/questions/3720/how-do-i-get-the-raw-private-key-from-my-mist-keystore-file)

```bash
kostas@kostas-validator:~/goerli/goerli-node/keystore$ python3
Python 3.8.5 (default, Jan 27 2021, 15:41:15)
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import hashlib
>>> dec_key = hashlib.scrypt(bytes('node-goerli', 'utf-8'), salt=bytes.fromhex('2d06aa483f1c7dc36bddb188b449b87868798a03679227254453e92e7385ab18'), n=262144, r=8, p=1, maxmem=2000000000, dklen=32)
>>> print(dec_key)## How to add new account with existed private key
b'\x9c\xac G\xe9\xb9\x16\xdb\xe3kjOe\xcfr\xa1\xe9\x1b\xf2tw\xc6\xb2\xd4i\xbe\xbbEX+\x1e~'
>>> validate = dec_key[16:] + bytes.fromhex('20eefb63e134b84067070485c30a49bbbf7add372974ade055c9ff7385c8e678')
>>> from Crypto.Hash import keccak
>>> keccak_hash=keccak.new(digest_bits=256)
>>> keccak_hash.update(validate)
<Crypto.Hash.keccak.Keccak_Hash object at 0x7fde7fa7ac70>
>>> print(keccak_hash.hexdigest())
3d8126c1a1602e5b6dbc4548dbebe2bdd5e028009b4d5b59925127b2422b434a
>>> from Crypto.Cipher import AES
>>> from Crypto.Util import Counter
>>> iv_int=int('a3de849989e8ab0f0e204c2af6ebe890', 16)
>>> ctr = Counter.new(AES.block_size * 8, initial_value=iv_int)
>>> dec_suite = AES.new(dec_key[0:16], AES.MODE_CTR, counter=ctr)
>>> plain_key = dec_suite.decrypt(bytes.fromhex('20eefb63e134b84067070485c30a49bbbf7add372974ade055c9ff7385c8e678'))
>>> print('0x' + plain_key.hex())
0x86eaa755826638cb0297cf1ad6d2fe2aa7a12b4a072ad2ff704636a3c01cb040
```

##### Add new account with this private key to local Parity node

```bash
$ curl --data '{"method":"parity_newAccountFromSecret","params":["0x86eaa755826638cb0297cf1ad6d2fe2aa7a12b4a072ad2ff704636a3c01cb040", "node-goerli"],"id":1,"jsonrpc":"2.0"}' -H "Content-Type: application/json" -X POST localhost:8540
```

Response:

```javascript
{
  "jsonrpc": "2.0",
  "result": "0xcdf4e0481e796afae76a9e4c537d4b895925b0cc",
  "id": 1
}
```

##### Check newly created account

```bash
$ curl --data '{"method":"personal_listAccounts","params":[],"id":1,"jsonrpc":"2.0"}' -H "Content-Type: application/json" -X POST localhost:8540
```

Response:

```javascript
{
 "jsonrpc":"2.0",
 "result":["0x004ec07d2329997267ec62b4166639513386f32e","0x00bd138abd70e2f00903268f3db08f2d25677c9e","0xcdf4e0481e796afae76a9e4c537d4b895925b0cc"],
 "id":1
}
```

## How to sign transaction

##### Unlock account

```bash
$ curl --data '{"method":"personal_unlockAccount","params":["0xcdf4e0481e796afae76a9e4c537d4b895925b0cc", "node-goerli", null],"id":1,"jsonrpc":"2.0"}' -H "Content-Type: application/json" -X POST localhost:8540
```

Response:

```javascript
{
  "jsonrpc":"2.0",
  "result":true,
  "id":1
}
```

##### Sign transaction

```bash
$ curl --data '{"id": 1,"jsonrpc": "2.0","method": "personal_signTransaction","params": [{"from": "0xcdf4e0481e796afae76a9e4c537d4b895925b0cc","to": "0xae94ab145613e9583065daf5ff909073ce37c1e6","gas": "0x6acfc0","value": "0x9184e72a","data": "0xe1c7392a"}, "node-goerli"]}'  -H "Content-Type: application/json" -X POST localhost:8540
```

Response:

```javascript
{
  "jsonrpc": "2.0",
  "result": {
    "raw": "0xf8688080836acfc094ae94ab145613e9583065daf5ff909073ce37c1e6849184e72a84e1c7392a2aa0fd20f4fb4d25a01a12b82631137c167e0f6b33852e817ce59c21294b06ea99a8a026e714f1b1d9bf030a2ad7b062dea7d27014d26a4bdb9d37d453d8f243936e13",
    "tx": {
      "blockHash": null,
      "blockNumber": null,
      "chainId": "0x3",
      "condition": null,
      "creates": null,
      "from": "0xcdf4e0481e796afae76a9e4c537d4b895925b0cc",
      "gas": "0x6acfc0",
      "gasPrice": "0x0",
      "hash": "0xf1e6c167398bda05cd9bc677237cf86b0582d896432da732b758b6fd581cc61c",
      "input": "0xe1c7392a",
      "nonce": "0x0",
      "publicKey": "0x81de0e52ff443dfb450af24214b033e74e93bdd962bbe27eda52f72d1c8a348fc4adbb09b15fb78c828d4492c3e68856ec620f9b9b61da102170ae08750e5f85",
      "r": "0xfd20f4fb4d25a01a12b82631137c167e0f6b33852e817ce59c21294b06ea99a8",
      "raw": "0xf8688080836acfc094ae94ab145613e9583065daf5ff909073ce37c1e6849184e72a84e1c7392a2aa0fd20f4fb4d25a01a12b82631137c167e0f6b33852e817ce59c21294b06ea99a8a026e714f1b1d9bf030a2ad7b062dea7d27014d26a4bdb9d37d453d8f243936e13",
      "s": "0x26e714f1b1d9bf030a2ad7b062dea7d27014d26a4bdb9d37d453d8f243936e13",
      "standardV": "0x1",
      "to": "0xae94ab145613e9583065daf5ff909073ce37c1e6",
      "transactionIndex": null,
      "v": "0x2a",
      "value": "0x9184e72a"
    }
  },
  "id": 1
}
```
## Full account import to local PoA network node from keystore file

Checkout more detailed info here: [importaccount.md](importaccount.md);

Full account import to primary local node for [local network](https://gitlab.com/pheix-research/ethereum-local-network) should be done with the next steps (example for address `0xcdf4e0481e796afae76a9e4c537d4b895925b0cc`):

* first of all you need to create full clone of [Görli test network](https://github.com/goerli/testnet) locally, eventually you need to patch `demo-spec.json`, set network identifier to `0x5`:

```javascript
    "params": {
        "gasLimitBoundDivisor": "0x400",
        "maximumExtraDataSize": "0x20",
        "minGasLimit": "0x5a995c0",
        "networkID" : "0x5",
        ...
    }
```

* run local PoA network with helper script `run-parity.sh`;
* clone the key file from your public node or wallet (**keep in mind that any work with key files has potential security risks and issues**);
* put key file to `/ethereum-local-network/parity.local/parity-node0/keys/DemoPoA`;
* new account will be added **on fly**, by the way run the next command to verify the previous step:

```bash
$ curl --data '{"method":"personal_listAccounts","params":[],"id":1,"jsonrpc":"2.0"}' -H "Content-Type: application/json" -X POST localhost:8540 | jq
```

---

##### Sign transaction locally

To sign transaction at local node you should:

* get `gasPrice` from Görli:

```bash
$ curl https://goerli.infura.io/v3/token -X POST -H "Content-Type: application/json" -d '{"jsonrpc":"2.0","method":"eth_gasPrice","params":[],"id":1}' | jq
```

* get `nonce` from Görli:

```bash
$ curl https://goerli.infura.io/v3/token -X POST -H "Content-Type: application/json" -d '{"id": 1,"jsonrpc": "2.0","method": "eth_getTransactionCount", "params": ["0xcdf4e0481e796afae76a9e4c537d4b895925b0cc","latest"]}' | jq
```

* sign transaction locally:

```bash
$ curl --data '{"id": 1,"jsonrpc": "2.0","method": "personal_signTransaction","params": [{"from": "0xcdf4e0481e796afae76a9e4c537d4b895925b0cc", "to": "0x38c927fee135d6a73cb87387707a1537b3684db1", "gas": "0x5b8d80", "gasPrice": "0x8cbb2370", "value": "0x0", "data": "0xe1c7392a", "nonce": "0x7519"}, "node-goerli"]}' -H "Content-Type: application/json" -X POST localhost:8540 | jq
```

---

##### Commit transaction to Görli

Use `raw` value from response (`personal_signTransaction`) as the parameter for `eth_sendRawTransaction`:

```bash
$ curl https://goerli.infura.io/v3/token -X POST -H "Content-Type: application/json" -d '{"jsonrpc":"2.0","method":"eth_sendRawTransaction","params":["0xf86a827519848cbb2370835b8d809438c927fee135d6a73cb87387707a1537b3684db18084e1c7392a2ea072b52387169beaca1b78a381c0add6da2ad7296c68adf4d712a91d2a7dc7e907a02d6c6d22ae55c3c2c8bd65ea35f7fc1921e95d8a97db6da051a5b731f109c4cc"],"id":1}'
```

Response:

```javascript
{
    "jsonrpc":"2.0",
    "id":1,
    "result":"0xc6ff090edfa0c3cd90596897522e5e478a81dc9d12992080f4614730a53d45f5"
}
```

Use `result` from response as transaction hash at [https://goerli.etherscan.io](https://goerli.etherscan.io) to check the transaction status (should be `Success`).

## Local node authentication schema

The idea is to use local PoA network node (run in Docker) also as auth node. On local node we can:

* restrict global permissions: user account or authority account;
* set time out for account unlock (e.g. 60 sec);
* regenerate tokens for each session/request (see [User accounts management](index.md#user-accounts-management));
* call `personal_unlockAccount` with token credentials;

Take a look to [https://jwt.io/](https://jwt.io/) - JSON Web Tokens.
