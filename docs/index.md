# PHEIX.io specifications

This is very early alfa draft of PHEIX.io (PIO) service specification. For now it just the set of requirements.

## PIO service generic requirements

* Work over JSON REST API
* User accounts management (public keys and tokens)
* User PoA endpoints management (infura, local (IPC), remote (RPC))
* Store data on blockchain (POST, PATCH, DELETE)
* Search data on blockchain (GET)

### Work over JSON REST API

The current implementation of `Pheix::Addons::Embedded::User` should be taken as an entry point. We shall use `/api` route as default. For now we have also [https://pheix.org/api-debug](https://pheix.org/api-debug) tool.

### User accounts management

We have `user` and `pass` keys at `storage` section at global [configuration file](https://gitlab.com/pheix-pool/core-perl6/-/blob/develop/www/conf/config.json#L218).

We should add `pkey` key and assign (at the beginning) user's public key to it. Public key could be generated with:

```bash
$ openssl rand -base64 64 | tr -d '\n'
```

Then we just have to encrypt `user` with this public key and generate token from this encrypted value.

```bash
$ KEY=`openssl rand -base64 64 | tr -d '\n'`
$ echo "0x54684973206953206d792064754d6d79206d7367" | openssl aes-256-cbc -pass pass:$KEY -a | tr -d '\n'
$ echo "U2FsdGVkX19bfSIS0r7ATqOTcltnjWiB/DRId+GtTizC0Sj+PXv4Y1Q5tz8C3p/3+GWVZKNWwpH3Mn6DmGoaSQ==" | base64 -d | openssl aes-256-cbc -d -pass pass:$KEY
# 0x54684973206953206d792064754d6d79206d7367
```

At the example below `$KEY` should be used as token:

```bash
$ echo $KEY
# 9no4gvTmms13SV+5nWDd1a2GYRTbm2SSt/dqSuI+r5RgAlJGuSaBjtw1+24R285HRGZw5oTPDeQNG00TB62+2Q==
```

### User PoA endpoints management

Current version of Pheix supports [Infura](https://infura.io/) and remote PoA RPC endpoints. As we have mentioned [here](https://gitlab.com/pheix-pool/core-perl6/-/issues/126), that we need to check the support of `Kovan` and `Rinkeby` test networks.

Also it's the deal to check local endpoints available via Unix sockets.

### Store and search data on blockchain

This functionality should be implemented in separate module `Pheix::Addons::IO::API`.

## Contributor

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
