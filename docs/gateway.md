# Multi network gateway

The important feature of the **ethelia.io** service is the option of multi network gateway. The idea is to select the primary storage network and populate records to other networks as the metadata or encrypted data parts.

**Must read** reference to Ethereum test networks details: [https://ethereum.stackexchange.com/a/30072](https://ethereum.stackexchange.com/a/30072).

## Abstract

Ethereum blockchain could be much more decentralized if we will use the mix of main net, public test nets and private nets. Such method makes it possible to increase the resilience of our distributed application against network attacks or their natural degradation.

By the way, when we use the mix of private and public nets, we also get the additional data access level: sensitive data is stored only on private net and open data — on public net. This approach is quite good for third-party audit or tracing.

To manage data in this heterogeneous environment we need data relays (or routers) providing transparent data flows from one net to another. Actually the private net is the basic one: we have full access to it, we can store as much data as we want and we can make it quite fast — just by deploying to muscle servers or high performance cloud.

At middle layer we have a few official Ethereum test nets, they are quite close to private net, but at least **x2** slower due to distributed and decentralized nature. We should use them to store meta data: specific headers, checksums or any details that could be useful for private net data inspection or restore. The general approach for data storing on tests nets is **mixing** and **shaking**: data should look (and actually be) chaotic, random and inconsistent.

The top layer: the Ethereum main net. It's non-free, so we should store as less data as we can just to make overall system cheaper. The idea is to store references (pointers) to data from test nets. Via these refs we can retrieve the relevant meta data sequence from test nets and trace or audit initial private net data.

We will try Pheix as relay for Ethereum networks, it's completely inspired and driven by Raku.

## Sample data model

```mermaid
graph TD;

SubGraph2C-->A[Store public metadata or encrypted data sets on public blockchains: Görli, Rinkeby, Kovan]
A-->B[Data in public networks should be used as prover for local PoA state consistency]
B-->C[Store metadata or just hashes of public data in test net in main net]
C-->D[Data in mainnet should be used as the index for data in test nets]

  subgraph "The primary local PoA network"
      SubGraph2Flow(Blockchain storage for tamper-proof data)
      SubGraph2Flow --> SubGraph2A[Full functional CRUD smart contract]
      SubGraph2A --> SubGraph2B[Full storage ledger with all data]
      SubGraph2B --> SubGraph2C[Full data and metadata audit/analyze]
  end
```

---

## Benefits

The major benefit **ethelia.io** service: we have an integration entity for local PoA, testnets and mainnet. The important thing is ability to audit and analyze tamper-proof data from local PoA network without any granted access to it.

This could be done with inspection of:

* refs (consistency) on mainnet;
* data (consistency) on test nets;
* metadata or encrypted data (audit/analyze) on testnets.

## Reverse data engineering on mainnet indexes

Main net indexes could be used for retrieving data from test nets. The questions:

* ok, we can store anything we want on test net, it's free - but how we can guarantee that stored data is covering the data set in local PoA network (spoiler: store CRC for local PoA data sets)?
* what's about encryption? Interesting thing: is we suppose **ethelia.io** service as data relay/router, it could be the service for retrieving data from both local and public networks, so we can store public keys in local PoA transactions and use them for linked data decryption (in test network).
* regarding previous question: if private key(s) will be leaked, what should we do? Encrypted data self destruction or whatever?
* the generic question: how will the auditor prove that local PoA state is valid on mainnet/testnet data?

## Automatic data update

User account at **ethelia.io** uses the next ETH addreses:

* Local PoA network addrs as **local**;
* Public testnet addrs (Goerli, Kovan, whatever) as **interim** (we can handle a few interim addrs — for each test network);
* Public mainnet addrs as **main**.

All user activity is inside Local PoA network. So **ethelia.io** API provides access to local PoA network. By the way user set **sync points**, where data from local PoA network syncs with test net(s) and main net automatically.

### Sync on subscription

Detailed info about how subscriptions works: https://geth.ethereum.org/docs/rpc/pubsub.

By the way **ethelia.io** service should subscribe to events from private net (it's simple, 'cause we have full access to local PoA network node) and populate event meta data to public test nets and main net on demand.

It looks clear, but we should have Pheix [subscription process alive](subscription.md) (maybe as daemon or service). Also we should have a monitor and simple statistics dumper/analyzer.

## Smart contracts

I suppose [generic CRUD smart contract](https://gitlab.com/pheix-research/smart-contracts) on local PoA and test nets. On main net we will use light version of generic CRUD smart contract: we will store just refs or short metadata of test net(s) records.

## How to earn profits

Economic model of **ethelia.io** has next options:

* **plan no.1:** local PoA (10,000 requests per day) — free;
* **plan no.2:** local PoA (10,000 requests per day) + Görli test net access — annual subscription (cheapest one);
* **plan no.3:** local PoA (10,000 requests per day) + all test nets access — monthly subscription (recommended one);
* **plan no.4:** local PoA (10,000 requests per day) + all test nets access + main net access — monthly subscription + fee for every transaction to main net.

## Technical details

<span style="color:#fff; background-color:#ff9d00; padding:5px;">**WIP**</span>: based on native **Pheix** with subscription service `Pheix::Service::Subscription` or smth similar.

### Mai net smart contract

In main net we should store:

* the transaction hash from testnet(s), maximum 3 refs/pointers (max 3 test nets are supported);
* the metadata from local PoA network: transaction hash, block number, account addr and [event details](https://gitlab.com/pheix-research/smart-contracts/-/blob/master/sol/PheixDatabase.sol#L6).

We have to figure out how should the generic CRUD smart contract be optimized or be simplified for this task.

<span style="color:#fff; background-color:#ff0018; padding:5px;">**TBD**</span>

#### Testing details

The approach is to use **Ropsten** as a main net simulator and **Goerli** as primary test net.

<span style="color:#fff; background-color:#ff0018; padding:5px;">**TBD**</span>
