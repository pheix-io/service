# Admin panel proposal

The early alpha implementation should cover the generic administration layer features and options:

1. [Authentication](index.md) with **login-password** on local Ethereum node, get session key (token) and start [session](session.md);
2. Display current statistics for site visitors, simple plots and tables for `bigbro.tnk`;
3. Display error logs, simple table for `logs.tnk`;

## Token as account address balance

It's good idea to use private PoA network account balance as token seed. To start with this model we need to prove that addr balance has at least 1 ETH.

Every auth request should generate a small delta amount in WEI and send this delta amount to **Authentication smart contract** (ASC) associated with the auth session.

If transaction is completed with successful status, it means successful token update, so we generate new pkey for account, encode token with it and send back in response.

### Multiple sessions

As we work with balance, there could be concurrency for multiple auth sessions. To prevent this, we will deny multiple sessions for security reasons. We should check if account is previously unlocked with `listWallets` at auth attempt and throw **Already authenticated** if account is unlocked.

## Authentication model

```mermaid
graph TD;

A[Login form for login and password] --> B[Post login-password pair via admin route];
B --> C[Get addr by login from settings];
C --> D[Try to auth with addr and password on local Ethereum node];
D --> SubGraphAuthOk;
SubGraphAuthOk8 --> E[Start a new session with token stored in cookie];
E --> SubGraphSession;
SubGraphSession9 --> F[Return encoded tx hash as new token and content by route];
F --> G[Wait for new request to admin layer]
G --> SubGraphSession;

subgraph "Authentication model with local Ethereum node";
    SubGraphAuthOk(Check if account has enough ETH and is locked);
    SubGraphAuthOk --> SubGraphAuthFails[401];
    SubGraphAuthOk --> SubGraphAuthOk2[Update and store pkey];
    SubGraphAuthOk2 --> SubGraphAuthOk3[Generate random delta in WEI];
    SubGraphAuthOk3 --> SubGraphAuthOk4[Calculate and encrypt new balance];
    SubGraphAuthOk4 --> SubGraphAuthOk5[Unlock account with account addr and pwd];
    SubGraphAuthOk5 --> SubGraphAuthFails[401];
    SubGraphAuthOk5 --> SubGraphAuthOk6[Send delta to ASC with encrypted bal & pwd];
    SubGraphAuthOk6 --> SubGraphAuthOk7[Wait for tx to be mined];
    SubGraphAuthOk7 --> SubGraphAuthOk8[Encode and return tx hash as token];
end;

subgraph "Authentication session";
    SubGraphSession(Decode token with pkey to transaction hash);
    SubGraphSession --> SubGraphSession1[Get account addr and data by transaction hash];
    SubGraphSession1 --> SubGraphSession2[Decrypt balance and pwd from data with pkey]
    SubGraphSession2 --> SubGraphSession3[Update and store pkey];
    SubGraphSession3 --> SubGraphSession4[Check account balance and unlocked status];
    SubGraphSession4 --> SubGraphSessionFails[401];
    SubGraphSession4 --> SubGraphSession5[Generate random delta in WEI];
    SubGraphSession5 --> SubGraphSession6[Calculate new balance];
    SubGraphSession6 --> SubGraphSession7[Unlock account with account addr and pwd];
    SubGraphSession7 --> SubGraphSessionFails[401];
    SubGraphSession7 --> SubGraphSession8[Send delta to ASC with encrypted bal & pwd];
    SubGraphSession8 --> SubGraphSession9[Wait for tx to be mined];
end;
```

---

## More about pkey storing and retrieval

The generic idea of `pkey` (**auth session public key**) storing and retrieval is:

* generate `pkey` at ASC on deployment step (auth session init): after successful login and password verification and successful auth on ethereum node;
* naive `pkey` generation proposal — <span style="color:#fff; background-color:#ff9d00; padding:5px;">**WIP**</span>;
* `pkey` should be stored in newly deployed smart contract.

The smart contract requirements:

* Implements simple storage for `bytes32` value;
* Read and write access only for contact owner;
* All smart contract methods should compare current `block.timestamp` with the initial timestamp;
* Initial timestamp is immutable and set up at the deployment step;
* Timestamps delta is the constant and hardcoded in smart contract source code, for now we have 10 minutes session;
* If the subtraction of current and initial timestamps is more than delta smart, contract should be self-destructed.

Some vulnerable improvements (this will make long-live auth session available):

* Initial timestamp will be considered as **current** and is mutable;
* Initial value for current timestamp is set up at the deployment step;
* We can/have to call `updateState` (this func updates current timestamp to `block.timestamp`) on with every access to `pkey`.

### Smart contract options

For current auth session we should use newly deployed smart contract as **Authentication smart contract** (ASC) and transfer funds to its address. This should be tested and if it works — we do not need to store BAA addr anywhere out of blockchain;

We can/have to update `pkey` on every retrieval request by calling `updateState` func, but it will make the session processing slower. I suppose this feature should be configurable.

### Smart contract implementation details

Naive **Authentication smart contract** (ASC): [https://gitlab.com/pheix-research/smart-contracts/-/commit/4d9c2a2a75682c67dcf20ba818fd8bfa93159d5b](https://gitlab.com/pheix-research/smart-contracts/-/commit/4d9c2a2a75682c67dcf20ba818fd8bfa93159d5b), related ticket: [https://gitlab.com/pheix-research/smart-contracts/-/issues/43](https://gitlab.com/pheix-research/smart-contracts/-/issues/43).

## Critics

The biggest vulnerability of **Authentication model** shown above is the `pkey`. If the authentication session is closed in dirty manner, e.g. application (browser, page, whatever) is closed, we still have valid `pkey` for the delta seconds and public transactions with encrypted password.

So, if someone will get the access to Ethereum node, he will be able to retrieve the `pkey` (as it stored at smart contract) and decrypt the node password from public transactions.

Of course, this is quite hard task, and obviously, if some will get the access to Ethereum node, he will get the password, so it's no need to decrypt it.

One more attack vector is to deploy the sattelite **Authentication smart contract** (right the same source code) from the attacker addr to the local PoA network where the auth node is.

Attack is:

* patch ASC with a few helper funcs;
* get the sattelite ASC deployed;
* monitor auth transactions from target addr;
* once we have found auth transaction (ASC deploying transaction or `updateState` transaction) we can grab `block.*` details from it and synthesize `pkey` at our sattelite ASC with helper funcs;
* once we get the `pkey` we can decrypt data from auth transaction: delta and password;
* we got the password and addr from auth node 😭😭😭.

## Strict approach

Do not store the password in transaction data. In this case we do not need auth smart contract at all. If the session is expired on Geth node, we will get session expired at administration layer. It means we have to start new session with explicit authentication.

I guess this option should be configurable and enabled by default.
