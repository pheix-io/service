# Signer

According the [authentication](auth.md) details, we can sign the transactions for test networks (eventually for mainnet also) on local PoA node. This entity/node is **signer**.

## Signer configuration at Pheix `config.json`

Just add a new storage for **signer** node:

```javascript
{
    "module": {
        "configuration": {
            "settings":  {
                "storage": {
                    "group": {
                        "blockchain-tx-signer": {
                            "type": "1",
                            "path": "",
                            "strg": "",
                            "extn": "",
                            "prtl": "http://",
                            "host": "127.0.0.1",
                            "port": "8541",
                            "hash": "",
                            "user": "0x00aa39d30f0d20ff03a22ccfc30b7efbfca597c2",
                            "pass": "node1",
                            "data": ""
                        },
                    }
                }
            }
        }
    }
}
```

## Simple signing transactions on Pheix side

On Pheix side we need to add `Pheix::Controller::Blockchain::Signer`:

* use **relay node** as `$target` — previously created `Pheix::Model::Database` object;
* create **signer** database object at `new('signer-storage', $target)` and call `sign($marshalledtx)` on it:

```perl
# prototype
method Pheix::Controller::Blockchain::Signer.new(Str :$signer!, Pheix::Model::Database :$target!);
method Pheix::Controller::Blockchain::Signer.sign(Str :$marshalledtx) returns Str;
method Pheix::Controller::Blockchain::Signer.send(Str :$rawtx) returns Str;

# usage
my $tx_signer = Pheix::Controller::Blockchain::Signer.new(:signer('blockchain-tx-signer'), target($dbobj));
my Str $dt    = $tx_signer.chainobj.ethobj.marshal($method, $data); # do marshaling
my Str $raw   = $tx_signer.sign(:marshalledtx($dt));
```

* all required data for transaction signing (`from`, `to`, `gas`, `gasprice`, `value`, `nonce` and `data`) should be taken from `$tx_signer` and `$tx_signer.target` objects or `$tx_signer.target` helper methods (like `$tx_signer.target.chainobj.ethobj.eth_gasPrice()`).
* send raw transaction with `send()`, if send is ok we should got transaction hash as return value, otherwise `0x0`.

Naive implementation: [Pheix::Controller::Blockchain::Signer](https://gitlab.com/pheix-pool/core-perl6/-/blob/744bfbbeac5a96516a742c998d9c9ae0fa342a76/lib/Pheix/Controller/Blockchain/Signer.rakumod), unit test (usage sample): [27-naive-signer.t](https://gitlab.com/pheix-pool/core-perl6/-/blob/744bfbbeac5a96516a742c998d9c9ae0fa342a76/t/27-naive-signer.t).

## Transparent (compatible mode) signing transactions on Pheix side

Add signer details to config and add signer reference to target table:

```javascript
{
    "module": {
        "configuration": {
            "settings":  {
                "storage": {
                    "group": {
                        "blockchain-tx-signer": {
                            "type": "1",
                            "path": "",
                            "strg": "",
                            "extn": "",
                            "prtl": "http://",
                            "host": "127.0.0.1",
                            "port": "8541",
                            "hash": "",
                            "user": "0x00aa39d30f0d20ff03a22ccfc30b7efbfca597c2",
                            "pass": "node1",
                            "data": ""
                        },
                        "rinkeby": {
                            "type": "1",
                            "path": "conf/system/abi",
                            "strg": "PheixDatabase",
                            "extn": "abi",
                            "prtl": "https://",
                            "host": "rinkeby.infura.io",
                            "port": "443/v3/110d18b2bc9f11eb85290242ac130003",
                            "hash": "0xd47613544f54dd26afe3edcae19e87c788b3ac321df0562244645d21af761001",
                            "user": "",
                            "pass": "",
                            "data": "undefined",
                            "sign": "blockchain-tx-signer"
                        }
                    }
                }
            }
        }
    }
}
```

Method `write_blockchain` method from `Pheix::Model::Database::Blockchain` should work in **send** and **sign** transaction modes. Mode **send** transaction is implemented now and mode **sign** is more complicated **send** mode: we do everything we're doing now, but sending is a little bit complicated: before send we signing tx on **signer** and then perform `sendRawTransaction` instead of `sendTransaction`.

Also we need to create signer object while creating `chainobj`. if `chainobj.signer` is defined we do transaction signing at `write_blockchain` — falling to **sign** mode.

Implementation:

1. `Pheix::Model::Database::Blockchain::SendTx` [module](https://gitlab.com/pheix-pool/core-perl6/-/blob/transparent_signer/lib/Pheix/Model/Database/Blockchain/SendTx.rakumod);
2. [Updated](https://gitlab.com/pheix-pool/core-perl6/-/blob/transparent_signer/lib/Pheix/Model/Database/Blockchain.rakumod#L133) `write_blockchain` method.
